# Connexion parameters
SOCKET_HOST = 'localhost'
SOCKET_PORT = 50019
SOCKET_MESSAGE_SIZE = 1843200

# Pepper variables
# pepper 1
PEPPER_1_IP = '10.77.3.15'
# pepper 2
PEPPER_2_IP = '10.77.3.16'
PEPPER_PORT = 9559
PEPPER_LANGUAGE = 'French'

# Messages for different commands
PEPPER_GET_IMAGE_COMMAND = b'GET_IMAGE'
PEPPER_SAY_COMMAND = b'PEPPER_SAY'
PEPPER_PLAY_MUSIC_COMMAND = b'PEPPER_PLAY'
PING_COMMAND = b'PING'
PONG_COMMAND = b'PONG'

# Pepper camera resolution
PEPPER_CAMERA_WIDTH = 240
PEPPER_CAMERA_HEIGHT = 320 
PEPPER_COLOR_CHANNELS = 3 
