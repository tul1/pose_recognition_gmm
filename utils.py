
BODY_POSE_DIM = 16
BODY_CENTER_DIM = 2
USED_BODY_KEYPOINTS_FULL = [0, 1, 2, 3, 4, 5, 6, 7, 8]
USED_BODY_KEYPOINTS_WITHOUT_HIP = [0, 1, 2, 3, 4, 5, 6, 7]

KEYPOINTS_DICT = {
    0: 'Nose',
    1: 'Neck',
    2: 'RShoulder',
    3: 'RElbow',
    4: 'RWrist',
    5: 'LShoulder',
    6: 'LElbow',
    7: 'LWrist',
    8: 'MidHip',
    9: 'RHip',
    10: 'RKnee',
    11: 'RAnkle',
    12: 'LHip',
    13: 'LKnee',
    14: 'LAnkle',
    15: 'REye',
    16: 'LEye',
    17: 'REar',
    18: 'LEar',
    19: 'LBigToe',
    20: 'LSmallToe',
    21: 'LHeel',
    22: 'RBigToe',
    23: 'RSmallToe',
    24: 'RHeel',
    25: 'Background'}

NORMALISED_KEYPOINTS_DICT = {
    0: 'Nose',
    1: 'RShoulder',
    2: 'RElbow',
    3: 'RWrist',
    4: 'LShoulder',
    5: 'LElbow',
    6: 'LWrist',
    7: 'MidHip'}

BODY_CENTER_KEYPOINT = 'Neck'

POSES = {
    0: 'MAIN_EN_L_AIR',
    1: 'SUR_LA_TETE',
    2: 'AUX_EPAULES',
    3: 'ET_EN_AVANT',
    4: 'BRAS_CROISES',
    5: 'SUR_LE_COTES',
    6: 'MOULINEZ',
    7: 'ET_ON_SE_TAIT'}


NORMALIZATION_METHODS = [
    'No_Normalization',
    'Nose_MidHip__RShoulder_LShoulder',
    'Nose_MidHip__Nose_MidHip',
    'Nose_MidHip__3xNose_MidHip',
    'RShoulder_LShoulder__RShoulder_LShoulder',
    'RShoulder_LShoulder__3xRShoulder_LShoulder',
    'Nose_Neck__RShoulder_LShoulder',
    'Nose_Neck__Nose_Neck',
    'Nose_Neck__3xNose_Neck']

MODEL_FILENAMES = [
    'Nose_MidHip__3xNose_MidHip_spherical.joblib',
    'Nose_MidHip__3xNose_MidHip_diag.joblib',
    'Nose_MidHip__3xNose_MidHip_tied.joblib',
    'Nose_MidHip__3xNose_MidHip_full.joblib',
    'RShoulder_LShoulder__RShoulder_LShoulder_spherical.joblib',
    'RShoulder_LShoulder__RShoulder_LShoulder_diag.joblib',
    'RShoulder_LShoulder__RShoulder_LShoulder_tied.joblib',
    'RShoulder_LShoulder__RShoulder_LShoulder_full.joblib',
    'Nose_MidHip__Nose_MidHip_spherical.joblib',
    'Nose_MidHip__Nose_MidHip_diag.joblib',
    'Nose_MidHip__Nose_MidHip_tied.joblib',
    'Nose_MidHip__Nose_MidHip_full.joblib',
    'Nose_MidHip__RShoulder_LShoulder_spherical.joblib',
    'Nose_MidHip__RShoulder_LShoulder_diag.joblib',
    'Nose_MidHip__RShoulder_LShoulder_tied.joblib',
    'Nose_MidHip__RShoulder_LShoulder_full.joblib',
    'No_Normalization_spherical.joblib',
    'No_Normalization_diag.joblib',
    'No_Normalization_tied.joblib',
    'No_Normalization_full.joblib',
    'RShoulder_LShoulder__3xRShoulder_LShoulder_spherical.joblib',
    'RShoulder_LShoulder__3xRShoulder_LShoulder_diag.joblib',
    'RShoulder_LShoulder__3xRShoulder_LShoulder_tied.joblib',
    'RShoulder_LShoulder__3xRShoulder_LShoulder_full.joblib',
    'Nose_Neck__RShoulder_LShoulder_spherical.joblib',
    'Nose_Neck__RShoulder_LShoulder_diag.joblib',
    'Nose_Neck__RShoulder_LShoulder_tied.joblib',
    'Nose_Neck__RShoulder_LShoulder_full.joblib',
    'Nose_Neck__Nose_Neck_spherical.joblib',
    'Nose_Neck__Nose_Neck_diag.joblib',
    'Nose_Neck__Nose_Neck_tied.joblib',
    'Nose_Neck__Nose_Neck_full.joblib',
    'Nose_Neck__3xNose_Neck_spherical.joblib',
    'Nose_Neck__3xNose_Neck_diag.joblib',
    'Nose_Neck__3xNose_Neck_tied.joblib',
    'Nose_Neck__3xNose_Neck_full.joblib']


COVARIANCE_TYPE = ['spherical', 'diag', 'tied', 'full']

DATA_DIM_FULL = 16
DATA_DIM_WITHOUT_HIP = 14


def get_keypoint(keypoint_name):
    return [key for key, value in KEYPOINTS_DICT.items()
            if value is keypoint_name][0]


def logger(class_name, class_attrs):
    logout = '< - '
    logout += 'ObjectType: '
    logout += class_name
    logout += ' - '
    for attr_name in class_attrs:
        logout += str(attr_name)
        logout += ": "
        logout += str(class_attrs[attr_name])
        logout += " - "
    logout += '>'
    return logout
