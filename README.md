# S5 project 2019 : Gaussian Mixture Model classifier for an imitation game using OpenPoseTF and Pepper.

## Summary 

The project aims to be the first steps for a development of a more general learn-by-imitation approach for autistic children. 
The goal is to simulate a well-known nursery rhyme named "Mains en l'air" to interact with the patients. 
The song is divided up in 8 sections, where the entertainer would assume a pre-defined posture that the kids should try to imitate.

![Poses](https://ibb.co/g4tJnpZ)

The project is built on top of three main pillars, namely:

* Body pose detection: Using a simple camera and OpenPose.
* Pose recognition: Using a Gaussian Mixuter Clasifier. 
* Robot control and synchronization: Using Noaqi API

Body pose detection was entirely done using publicly available software. 

## Installation and Setup

The code is fully available [here](https://gitlab.com/tul1/pose_recognition_gmm/). The repository should be cloned to the source directory.
The repository above contains several scripts that allows the developper to create a dataset from jpg images, train and getting a GMM classifier and launching the game.
Several pre-train classifiers are availables in the models directory.

You need dependencies below.

* python3
* python2.7
* tensorflow 1.4.1+
* opencv3, protobuf, python3-tk
* slidingwindow
* [https://github.com/adamrehn/slidingwindow](https://github.com/adamrehn/slidingwindow)
* [https://github.com/ildoonet/tf-pose-estimation](https://github.com/ildoonet/tf-pose-estimation)

Once you have the dependencies installed you'll need to run the following command :

pip3 install -r requirements.txt

## Building de dataset

To build the dataset from the images run the script:

python3 process_pictures.py

You'll find the process raw images, processed images and processed images text file under the dataset directory.

## Train the GMM and getting the models

To train the GMM you'll need to run the following command:

python3 train_GMM.py

This script include the following: 
* --mode : 'full/_mode' or 'no/_hip'
* --input-file : dataset input file path.
* --output-file : Models output file path.
* --dateset-size : pictures per pose. By default this number is 30.
* --plot-data : when declaring this flag every graph is going to be shown.
* --help : to get some help

By running train_GMM.py without arguments you'll obtain a all the models in the models directorys followed by a report file explai

## Running the game

To run the game you'll need to run separetely the two following lines:

python3 client_game.py

python2.7 server_game.py

The python client script include the following arguments :
* --source: 0 for webcam otherwise path to the video, path for a video or 'pepper' to use the robot.
* --open-pose-model: OpenPose process pre trainned model cmu / mobilenet_thin / mobilenet_v2_large / mobilenet_v2_small.
* --normalization-method: Normalization method, this will depend on the chosen model.
* --gmm-model: GMM model file path.
* --data-dim: Either 14 or 16 depending on the GMM model.
* --help : to get some help.

## Game Flow

The expected behaviour is as follows:

* Pepper says hello and invites the patient to play with him.
* The music starts playing and the game goes into its main loop.
* Now, two possible scenarios can follow:
** The player adopts the right pose similar enough. Then Pepper will encourage him to keep going, and the song continues.
** The player does not make a similar pose, or the body pose is not detected properly. In this case, Pepper will ask the player to focus and try again, and the music will go back to the previous part.
* When all the gestures are done and the music ends, Pepper thanks the user and the game goes back to its first state. The user can then click on the tablet again to re-start the game without having to input his name again.


## Contact

Patricio Tula
tula.patricio@gmail.com