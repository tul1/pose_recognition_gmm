import numpy as np
from sklearn.mixture import GaussianMixture
from sklearn.model_selection import StratifiedKFold
from joblib import dump, load

from utils import POSES, logger, NORMALIZATION_METHODS

MODEL_PATH = './models/'

class PoseRecognitionGMM:
    def __init__(
            self,
            data=None,
            covariance=None,
            save_path=None,
            load_path=None):
        self.load_path_ = load_path
        self.data_ = []
        self.target_ = np.array([])
        self.covariance_ = covariance
        self.stats_ = {}
        self.save_path_ = save_path
        self.estimator_ = None  
        if self.load_path_:
            self.estimator_ = load(self.load_path_)
        else:
            # sort by data classes
            for training_pose in data:
                for pose_key, pose_name in POSES.items():
                    if training_pose is pose_name:
                        target = np.dot(
                            np.ones(len(data[training_pose])), pose_key)
                        self.target_ = np.concatenate((self.target_, target))
                        break
            self.target_ = np.sort(self.target_)
            # flat the data
            for pose_key, pose_name in POSES.items():
                for pose_data in data[pose_name]:
                    self.data_.append(pose_data.get_pose())
            self.data_ = np.array(self.data_)
            # Fold the data
            skf = StratifiedKFold(n_splits=4)
            skf.get_n_splits(self.target_)
            train_index, test_index = next(
                iter(skf.split(self.data_, self.target_)))
            # Split into train and test sets
            self.X_train_ = self.data_[train_index]
            self.y_train_ = self.target_[train_index]
            self.X_test_ = self.data_[test_index]
            self.y_test_ = self.target_[test_index]
            # Number of clases
            n_classes = len(np.unique(self.y_train_))
            # Create the GMM estimator
            self.estimator_ = GaussianMixture(
                n_components=n_classes,
                covariance_type=self.covariance_,
                init_params='random',
                max_iter=20,
                random_state=0)
            # Calculate the mean of each vector set the mean of the gaussian model
            self.estimator_.means_init = np.array(
                [self.X_train_[self.y_train_ == i].mean(axis=0) for i in range(n_classes)])

    def __repr__(self):
        return logger('PoseRecognitionGMM',
                      {'data_': self.data_,
                       'target_': self.target_,
                       'covariance': self.covariance_,
                       'stats_': self.stats_,
                       'load_path_': self.load_path_,
                       'save_path_': self.save_path_, })

    def get_save_path(self):
        return self.save_path_

    def get_load_path(self):
        return self.load_path_

    def set_save_path(self, path):
        self.save_path_ = path

    def set_load_path(self, path):
        self.load_path_ = path

    def train_GMM(self):
        self.estimator_.fit(self.X_train_)

    def load_model_new_model(self):
        estimator = load(self.load_path_)
        return estimator

    def get_model_stats(self):
        y_train_pred = self.estimator_.predict(self.X_train_)
        train_accuracy = np.mean(
            y_train_pred.ravel() == self.y_train_.ravel()) * 100
        self.stats_['train_accuracy'] = train_accuracy

        y_test_pred = self.estimator_.predict(self.X_test_)
        test_accuracy = np.mean(
            y_test_pred.ravel() == self.y_test_.ravel()) * 100
        self.stats_['test_accuracy'] = test_accuracy
        return self.stats_

    def save_model(self):
        dump(self.estimator_, MODEL_PATH + self.save_path_)

    def predict_pose(self, human_pose_normalized):
        human_pose_normalized_list = [human_pose_normalized.tolist()]
        human_pose_normalized = np. array(human_pose_normalized_list)
        result = self.estimator_.predict(human_pose_normalized)
        return POSES[result[0]]
