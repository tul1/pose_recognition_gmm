from utils import POSES

#General states and configurations
PLAYING_GAME_STATUS = 'PLAYING'
GAME_OVER_STATUS = 'GAME_OVER'
MODE_1 = 'MODE_1'
MODE_2 = 'MODE_2'

# General messages
# Greetings
START_GAME_QUOTE_BEGIN = b'Salut '
START_GAME_QUOTE_END = b', chantons une quantine ensemble!'
# Congratulations
CONGRATULATIONS_QUOTES = [b'Trais bien! tu as reussi a bien faire le geste!',
                          b'Nickel! tu as reussi a bien faire le geste!',
                          b'Excelent! tu as reussi a bien faire le geste!']
# Say good by
END_GAME_QUOTES = b'Bravo! tu as bien fait toutes les gestes jusqu\'a la fin'

#MODE 1
GAMES_STATES_MODE_1 = POSES
# Quotes per pose
# GAME_PLAYLIST_MODE_1 = {
#     0: 'mains_en_l_air.mp3',
#     1: 'sur_la_tete.mp3',
#     2: 'aux_epaules.mp3',
#     3: 'et_en_avant.mp3',
#     4: 'bras_croises.mp3',
#     5: 'sur_les_cotes.mp3',
#     6: 'moulinet.mp3',
#     7: 'et_on_se_tait.mp3'}

GAME_PLAYLIST_MODE_1 = ['mains_en_l_air.mp3', 
                        'sur_la_tete.mp3',
                        'aux_epaules.mp3',
                        'et_en_avant.mp3', 
                        'bras_croises.mp3', 
                        'sur_les_cotes.mp3', 
                        'moulinet.mp3',
                        'et_on_se_tait.mp3']

#MODE 1
# Quotes per pose
# GAME_PLAYLIST_MODE_2 = {
#     0: 'mains_en_l_air.mp3',
#     1: 'sur_la_tete.mp3',
#     2: 'et_en_avant.mp3',
#     3: 'bras_croises.mp3',
#     4: 'sur_les_cotes.mp3',
#     5: 'moulinet.mp3',
#     6: 'et_on_se_tait.mp3'}

GAME_PLAYLIST_MODE_2 = ['mains_en_l_air.mp3', 
                        'sur_la_tete.mp3',
                        'et_en_avant.mp3', 
                        'bras_croises.mp3', 
                        'sur_les_cotes.mp3', 
                        'moulinet.mp3',
                        'et_on_se_tait.mp3']

GAMES_STATES_MODE_2 = {
    0: 'MAIN_EN_L_AIR',
    1: 'SUR_LA_TETE',
    2: 'ET_EN_AVANT',
    3: 'BRAS_CROISES',
    4: 'SUR_LE_COTES',
    5: 'MOULINEZ',
    6: 'ET_ON_SE_TAIT'}
