import socket
import argparse
from naoqi import ALProxy
import qi
import vision_definitions
from pepper_server_commons import *
from game_commons import GAME_PLAYLIST_MODE_1 as pepper_playlist

class PepperServer:
    def __init__(self,session, pepper_ip):
        self.pepper_ip_ = pepper_ip
        # Setup socket configuration
        self.socket_ = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket_.bind((SOCKET_HOST, SOCKET_PORT))
        self.socket_.listen(1)
        self.conn_, self.addr_ = self.socket_.accept()
        print('Connected by', self.addr_)
        # Disable Peppers AutonomousLife mode
        self.autonomous_mode_ = ALProxy("ALAutonomousLife", self.pepper_ip_, PEPPER_PORT)
        self.autonomous_mode_.setState("disabled")
        # Pepper stand up
        self.posture_service_ = session.service("ALRobotPosture")
        self.posture_service_.goToPosture("StandInit", 1.0)
        # video streaming
        self.video_service_ = session.service("ALVideoDevice")
        self.cameraID_ = 0
        self.img_client_ = ""
        self.register_image_client()
        # play music
        self.audio_player_service_ = session.service("ALAudioPlayer")

    def register_image_client(self):
        resolution = vision_definitions.kQVGA  # 320 * 240
        colorSpace = vision_definitions.kRGBColorSpace
        self._img_client_ = self.video_service_.subscribe("_client", resolution, colorSpace, 5)
        # Select camera
        self.video_service_.setParam(vision_definitions.kCameraSelectID, self.cameraID_)

    def unregister_image_client(self):
        if self.img_client_ != "":
            self.video_service_.unsubscribe(self.img_client_)

    def __del__(self):
        self.unregister_image_client()
        self.autonomous_mode_.setState("solitary")
        if self.conn_:
            self.conn_.close()

    def spin_the_wheel(self):
        while True:
            data = self.conn_.recv(SOCKET_MESSAGE_SIZE)
            #if there is no data let's break the socket connection
            if not data: break
            # call particular command
            pepper_answer = self.call_pepper_command(data)
            # socket answer
            if pepper_answer == None:
                pepper_answer = PONG_COMMAND
            self.conn_.sendall(pepper_answer)
        self.conn_.close()

    def pepper_play_music(self, file_name):
        print(file_name)
        if file_name in pepper_playlist:
            music_record_path = "/home/nao/playlist/" + str(file_name)
            fileId = self.audio_player_service_.loadFile(music_record_path)
            self.audio_player_service_.play(fileId)
        return PONG_COMMAND + b'_' + PEPPER_PLAY_MUSIC_COMMAND

    def pepper_say_something(self, something):
        tts = ALProxy("ALTextToSpeech", self.pepper_ip_, PEPPER_PORT)
        tts.say(str(something), PEPPER_LANGUAGE) 
        return PONG_COMMAND + b'_' + PEPPER_SAY_COMMAND

    def pepper_stream_video(self):
        image = self.video_service_.getImageRemote(self._img_client_)
        if image:
            image = image[6] # Naoqi camera data
            return image
        else:
            return PONG_COMMAND + b'_' + PEPPER_PLAY_MUSIC_COMMAND

    def call_pepper_command(self, cmd):
        cmd = cmd.split('|')
        if cmd[0] == PING_COMMAND:
            print(cmd[0])
            return PONG_COMMAND
        elif cmd[0] == PEPPER_GET_IMAGE_COMMAND:
            print(cmd[0])
            answer = self.pepper_stream_video()
            return answer
        elif cmd[0] == PEPPER_SAY_COMMAND:
            print(cmd)
            answer = self.pepper_say_something(cmd[1])
            return answer
        elif cmd[0] == PEPPER_PLAY_MUSIC_COMMAND:
            print(cmd[1])
            answer = self.pepper_play_music(cmd[1])
            return answer

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='pepper_socket_interface')
    parser.add_argument('--robot', type=str, default='pepper1', help='Robot name, either pepper1 or pepper2')
    args = parser.parse_args()
    
    # Select the robot IP
    pepper_ip = ''
    if args.robot is 'pepper1':
        pepper_ip = PEPPER_1_IP
    else:
        pepper_ip = PEPPER_2_IP

    # Initiate the naoqi session to use its API
    session = qi.Session()
    try:
        session.connect("tcp://" + pepper_ip + ":" + str(PEPPER_PORT))
    except RuntimeError:
        print("Can't connect to Naoqi at ip \"" + pepper_ip + "\" on port " + str(PEPPER_PORT) +".\n"
               "Please check your script arguments. Run with -h option for help.")

    # Pepper socket rutine
    pepper_server = PepperServer(session, pepper_ip)
    pepper_server.spin_the_wheel()