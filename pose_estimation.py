from tf_pose.estimator import TfPoseEstimator
from tf_pose.networks import get_graph_path

class PoseEstimation:
    def __init__(self, model='mobilenet_thin'):
        # cmu / mobilenet_thin / mobilenet_v2_large / mobilenet_v2_small
        self.model_ = model
        self.estimator_ = TfPoseEstimator(get_graph_path(self.model_), target_size=(320, 240))

    def get_estimator(self):
        return self.estimator_

    def get_humans(self, image):
        humans = self.estimator_.inference(image, resize_to_default=False, upsample_size=4.0)
        return humans

    def draw_humans(self, image, humans):
        return TfPoseEstimator.draw_humans(image, humans, imgcopy=False)

