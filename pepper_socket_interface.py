import socket
import cv2
import numpy as np
from pepper_server_commons import *

class PepperSocketInterface:
    def __init__(self, which_pepper):
        self.pepper_ip_ = ''
        if which_pepper is 'pepper1':
            self.pepper_ip_ = PEPPER_1_IP
        else:
            self.pepper_ip_ = PEPPER_2_IP
        self.socket_ = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket_.connect((SOCKET_HOST, SOCKET_PORT))
        self.send_ping()
    
    def __del__(self):
        if self.socket_: 
            self.socket_.close()

    def decode_image(self, image):
        print(len(image))
        if len(image) == 230400:
            nparr = np.frombuffer(bytes(image), np.uint8)
            nparr.shape = (PEPPER_CAMERA_WIDTH, PEPPER_CAMERA_HEIGHT, PEPPER_COLOR_CHANNELS)
            nparr = cv2.cvtColor(nparr, cv2.COLOR_BGR2RGB)
            return nparr
        return None

    def get_pepper_video(self):
        # Send request
        self.socket_.sendall(PEPPER_GET_IMAGE_COMMAND)
        # Get response
        response = self.socket_.recv(SOCKET_MESSAGE_SIZE)
        if response != PONG_COMMAND:
            image = self.decode_image(response)
            return image
        else: 
            return response

    def pepper_say(self, something):
        # Send request
        cmd = PEPPER_SAY_COMMAND + b'|' + something
        self.socket_.sendall(cmd)
        # Get response
        response = self.socket_.recv(SOCKET_MESSAGE_SIZE)
        return response

    def pepper_play_song(self, song_file):
        # Send request
        cmd = PEPPER_PLAY_MUSIC_COMMAND + b'|' + bytes([ ord(p) for p in song_file ])
        self.socket_.sendall(cmd)
        # Get response
        response = self.socket_.recv(SOCKET_MESSAGE_SIZE)
        return response

    def send_ping(self):
        print('sending ping')
        # Send request
        self.socket_.sendall(PING_COMMAND)
        # Get response
        response = self.socket_.recv(SOCKET_MESSAGE_SIZE)
        return response
