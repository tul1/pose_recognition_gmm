from utils import POSES
from game_commons import *
from pepper_socket_interface import PepperSocketInterface

class PoseGame:
    def __init__(self, pepper_interface, mode=MODE_1, player_name='human'):
        self.player_name_ = self.str_to_bytes(player_name)
        self.current_game_state_ = None
        self.pepper_interface_ = pepper_interface
        self.game_status_ = None
        self.pose_num_ = 0
        self.current_game_state_ = None
        self.pepper_playlist_ = None
        if mode is MODE_1:
            self.game_states_ = GAMES_STATES_MODE_1
            self.pepper_playlist_ = GAME_PLAYLIST_MODE_1 
        else:
            self.game_states_ = GAMES_STATES_MODE_2
            self.pepper_playlist_ = GAME_PLAYLIST_MODE_2

    def str_to_bytes(self, s):
        return bytes([ ord(p) for p in s ])

    def game_out(self, msg, speak):
        if self.pepper_interface_:
            if speak is True:
                self.pepper_interface_.pepper_say(msg)
            else:
                self.pepper_interface_.pepper_play_song(msg)

    def start(self):
        self.current_game_state_ = 0
        self.game_status_ = PLAYING_GAME_STATUS
        greetings = START_GAME_QUOTE_BEGIN + self.player_name_ + START_GAME_QUOTE_END
        self.game_out(greetings, speak=True)
        self.game_out(self.pepper_playlist_[self.current_game_state_], speak=False)

    def play(self, pose):
        print('actual state: ' + self.game_states_[self.current_game_state_])                
        print('Detected pose: ' + pose)
        if pose is self.game_states_[self.current_game_state_]:
            print('good pose!')
            self.set_next_state()
        return self.game_status_

    def set_next_state(self):
        self.current_game_state_ += 1
        if self.current_game_state_ is len(self.pepper_playlist_):
            self.game_out(END_GAME_QUOTES, speak=True)
            self.game_status_ = GAME_OVER_STATUS
        elif self.current_game_state_ >= 1 and self.current_game_state_ < len(self.pepper_playlist_):
            print(self.current_game_state_)
            self.game_out(CONGRATULATIONS_QUOTES[self.current_game_state_%3], speak=True)
            self.game_out(self.pepper_playlist_[self.current_game_state_], speak=False)
            self.game_status_ = PLAYING_GAME_STATUS
